const fs = require ('fs');


function writeUserDataToFile(userData){
  var jsonUserData = JSON.stringify(userData);

  fs.writeFile("./mock_usuarios.json",jsonUserData,"utf8",
    function(err){
      if(err){
        console.log(err);
      } else {
        console.log("Datos escritos en fichero");
      }
    }
  )
}

module.exports.writeUserDataToFile = writeUserDataToFile;
