const io = require('../io');
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudpr6ed/collections/";
const mLabAPIKey = "apiKey=5ErqEthZdY9RSLgg2J3vjNY7W34SM96c";
const requestJson = require('request-json');

function loginV1(req,res){
  console.log("POST /apitechu/v1/login");
  console.log ("email: " + req.body.email);
  console.log ("password: " + req.body.password);

  var users = require('../mock_usuarios.json');
  var result = {};
  var encontrado = false;

  for (user of users){
    if (user.email==req.body.email){
      encontrado=true;
      if(user.password==req.body.password){
        result.msg = "El usuario " + req.body.email + " se ha logado";
        user.logged = "true";
        io.writeUserDataToFile(users);
      }else{
        result.msg = "La password del usuario " + req.body.email + " no es correcta";
      }

      break;
    }
  }

  if(!encontrado){
    console.log("El usuario " + req.body.email + " no existe.");
    result.msg = "El usuario " + req.body.email + " no existe";
  }

  res.send(result);
}

function logoutV1(req,res){
  console.log("POST /apitechu/v1/logout");
  console.log ("idUsuario: " + req.body.id);

  var users = require('../mock_usuarios.json');
  var result = {};
  var resultado = false;

  for (user of users){
    if (user.id==req.body.id){
      encontrado=true;
      if(user.logged=="true"){
        result.msg = "Logout correcto del usuario " + req.body.id;
        result.id = req.body.id;
        delete user.logged;
        io.writeUserDataToFile(users);
      }else{
        result.msg = "Error en el logout del usuario";
      }
      break;
    }
  }

  if(!encontrado){
    result.msg = "Error en el logout del usuario";
  }

  res.send(result);
}

function loginV2(req,res){
  console.log("PUT /apitechu/v2/login/");

  var email = req.body.email;
  var query = 'q={"email" : "' + email + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + mLabAPIKey + "&" + query,
    function (err, resMLab, body){
      if(err){
        var response={
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
        res.send(response);
      } else {
          if (body.length > 0){

            if (crypt.checkPassword(req.body.password,body[0].password)){
              console.log("Clave de usuario verificada correctamente");
              var putBody = '{"$set":{"logged":true}}';

              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function (err, resMLab, body){
                  if(err){
                    var respuesta={
                      "msg" : "Error logando el usuario"
                    };
                    res.status(500);
                    res.send(respuesta);
                  }
                  else{
                    var respuesta={
                      "msg" : "Usuario logado con éxito"
                    };
                    res.send(respuesta);
                  }
                }
              )
            }
            else {
              var response={
                "msg":"La clave del usuario es incorrecta"
              };
              res.send(response);
            }
          }
          else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
            res.send(response);
        }
      }
    }
  );
}

function logoutV2(req,res){
  console.log("PUT /apitechu/v2/logout/");

  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + mLabAPIKey + "&" + query,
    function (err, resMLab, body){
      if(err){
        var response={
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
          if (body.length > 0){
            var putBody = '{"$unset":{"logged":""}}';

            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function (err, resMLab, body){
                if(err){
                  var respuesta={
                    "msg" : "Error haciendo logout del usuario"
                  };
                  res.status(500);
                  res.send(respuesta);
                }
                else{
                  var respuesta={
                    "msg" : "El usuario ha hecho logout con éxito"
                  };
                  res.send(respuesta);
                }
              }
            );
          }
          else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
            res.send(response);
        }
      }
    }
  );
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
module.exports.loginV2 = loginV2;
