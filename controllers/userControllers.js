const io = require('../io');
const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechudpr6ed/collections/";
const mLabAPIKey = "apiKey=5ErqEthZdY9RSLgg2J3vjNY7W34SM96c";
const requestJson = require('request-json');

function getUsersV1(req,res){
  console.log("GET /apitechu/v1/users");

  var result = {};
  var top = req.query.$top;
  console.log("Parámero top = " + top);

  var count = req.query.$count;
  console.log("Parámetro count = " + count);

  var users = require('../mock_usuarios.json');

  if (count=="true"){
    result.count = users.length;
    console.log(result.count);
  }

  if (top!=null){
    var resUsers = users.slice(0,(top));
    users = resUsers;
  }

  result.users = users;

  res.send(result);
}

function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");
  console.log(req.body);
  console.log ("first_name: " + req.body.first_name);
  console.log ("last_name: " + req.body.last_name);
  console.log ("email: " + req.body.email);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
  }

  var users = require('../mock_usuarios.json');
  users.push(newUser);

  io.writeUserDataToFile(users);
  console.log("Usuario añadido con éxito");

  res.send({"msg":"Usuario añadido con éxito"});
}

function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");

  var users = require('../mock_usuarios.json');

  for(user of users){
    if(user.id == req.params.id){
      console.log("Usuario " + user.id + " borrado");
    }
  }

  io.writeUserDataToFile(users);

  res.send({"msg":"Usuario borrado con éxito"});
}

function getUsersV2(req,res){
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Ciente creado");
  httpClient.get("user?" + mLabAPIKey,
    function (err, resMLab, body){
      var response = !err ? body :{
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserByIdV2(req,res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Ciente creado");
  httpClient.get("user?" + mLabAPIKey + "&" + query,
    function (err, resMLab, body){
      if(err){
        var response={
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
          if (body.length > 0){
            response = body[0];
          }
          else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
        }
      }
      res.send(response);
    }
  );
}

function createUserV2(req,res){
  console.log("POST /apitechu/v2/users");
  console.log ("first_name: " + req.body.first_name);
  console.log ("last_name: " + req.body.last_name);
  console.log ("email: " + req.body.email);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Ciente creado");
  httpClient.post("user?" + mLabAPIKey, newUser,
    function (err, resMLab, body){
      console.log("Usuario creado con éxito");
      res.send({"msg" : "Usuario creado con éxito"})
    }
  );
}



module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
